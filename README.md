# Hostmaker Challenge

## Web App
[http://isnifer-hostmaker.ddns.net/](http://isnifer-hostmaker.ddns.net/)

## What commands are available

```sh
npm run ci # will run ci.sh script — it will install root modules, bootstrap packages via lerna
npm run server-ci # this command helps me do faster rebuild code on the server
npm run clean # remove all modules from packages/*
npm run bootstrap # install modules for all packages/*
npm run clean:bootstrap # mixup previous commands
npm run api:test # run unit tests for action and reducer on api
npm run app:start # start development server for RN App
npm run app:cold-start # run previous command without cache
npm run app:ios # alias for react-native run-ios with build folder cleaning
npm run app:android # alias for react-native run-android with build folder cleaning
npm run web:start # start a webpack-dev-server with webapp (http://localhost:8080)
npm run web:build # build a web app to /packages/web/dist
npm run web:test # run E2E-tests for web app on http://isnifer-hostmaker.ddns.net
npm run server:start # run express server to delivery static content and json for API
```
