#!/bin/bash

isSeleniumAvailable() {
  [[ -x "$(command -v selenium-server)" ]]
}

isChromeDriverAvailable() {
  [[ -x "$(command -v chromedriver)" ]]
}

killSelenium() {
  pkill -9 -f selenium-server-standalone
}

# check selenium-server
if isSeleniumAvailable; then
  echo "selinium-server is available"
else
  brew install selenium-server-standalone
fi

# check chromedriver
if isChromeDriverAvailable; then
  echo "chromedriver is available"
else
  brew install chromedriver
fi

selenium-server > selenium.log 2>&1 &

npx tape -r ./__tests__/utils/setup.js -r isomorphic-fetch __tests__/*.test.js | tap-diff
