import React from 'react'
import PropTypes from 'prop-types'
import { addressParamsOrder } from 'shared/constants'

const addressParams = addressParamsOrder.map(parameter => [parameter, PropTypes.string.isRequired])

export default function Address({ address }) {
  return addressParams.reduce((memo, [item]) => {
    const currentProperty = address[item]

    return !currentProperty ? memo : memo.concat(
      <div key={item}>{currentProperty}</div>
    )
  }, [])
}

Address.propTypes = {
  address: PropTypes.shape(
    addressParams.reduce(
      (memo, [key, value]) => ({ ...memo, [key]: value }), {}
    )
  ).isRequired,
}
