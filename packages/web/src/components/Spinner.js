import React from 'react'

export default function Spinner() {
  return (
    <div style={styles.spinner}>
      <div style={{ ...styles.inner, ...styles.innerOne }} />
      <div style={{ ...styles.inner, ...styles.innerTwo }} />
      <div style={{ ...styles.inner, ...styles.innerThree }} />
    </div>
  )
}

const styles = {
  spinner: {
    display: 'inline-block',
    position: 'relative',
    width: 64,
    height: 64,
  },
  inner: {
    display: 'inline-block',
    position: 'absolute',
    left: 6,
    width: 13,
    background: '#D3D3D3',
    animation: 'spinner 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite',
  },
  innerOne: {
    left: 6,
    animationDelay: '-0.24s',
  },
  innerTwo: {
    left: 26,
    animationDelay: '-0.12s',
  },
  innerThree: {
    left: 45,
    animationDelay: 0,
  },
}
