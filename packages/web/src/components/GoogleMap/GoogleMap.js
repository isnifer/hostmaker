import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  GoogleMap as MapGoogle,
  Circle,
  withGoogleMap,
} from 'react-google-maps'
import Marker from './Marker'

class GoogleMap extends Component {
  state = {
    visibleInfoBox: null,
  }

  checkMarkerInsideCircle = (latLngObject) => {
    const circle = this.circleRef

    if (!circle) {
      return true
    }

    const bounds = circle.getBounds()
    const radius = circle.getRadius()

    const latLng = {
      lat: () => latLngObject.lat,
      lng: () => latLngObject.lng,
    }

    return (
      bounds.contains(latLngObject) &&
      google.maps.geometry.spherical.computeDistanceBetween(circle.getCenter(), latLng) <= radius
    )
  }

  handleMarkerClick = (markerId) => {
    this.setState(prevState => ({
      visibleInfoBox: markerId === prevState.visibleInfoBox ? null : markerId,
    }))
  }

  render() {
    const { center, markers } = this.props
    const circleOptions = {
      strokeWeight: 1,
      strokeColor: '#005005',
      fillColor: '#a5d6a7',
    }

    return (
      <MapGoogle defaultZoom={9} defaultCenter={center}>
        <Circle
          center={center}
          radius={center.radius}
          options={circleOptions}
          ref={node => this.circleRef = node}
        />
        {markers.map(marker => (
          <Marker
            key={marker.airbnbId}
            infoBoxIsVisible={this.state.visibleInfoBox === marker.airbnbId}
            onClick={this.handleMarkerClick}
            insideArea={this.checkMarkerInsideCircle(marker.position)}
            {...marker}
          />
        ))}
      </MapGoogle>
    )
  }
}

GoogleMap.propTypes = {
  center: PropTypes.shape({
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
  }).isRequired,
  markers: PropTypes.array,
}

GoogleMap.defaultProps = {
  markers: [],
}

export default withGoogleMap(GoogleMap)
