import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Marker as MapMarker } from 'react-google-maps'
import InfoBox from 'react-google-maps/lib/components/addons/InfoBox'
import startCase from 'lodash/startCase'

const getMarkerUrl = pinColor => (
  `http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|${pinColor}`
)

export default class Marker extends Component {
  handleClick = () => {
    this.props.onClick(this.props.airbnbId)
  }

  render() {
    const { position, owner, address, insideArea } = this.props

    if (!position.lat && !position.lng) {
      return null
    }

    const pinImage = new google.maps.MarkerImage(getMarkerUrl(insideArea ? '3bc250' : 'fe8074'),
      new google.maps.Size(21, 34),
      new google.maps.Point(0, 0),
      new google.maps.Point(10, 34)
    )

    const infoBoxStyle = {
      ...styles.infoBoxContainer,
      border: `1px solid ${insideArea ? 'transparent' : 'red'}`,
    }

    return (
      <MapMarker clickable icon={pinImage} position={position} onClick={this.handleClick}>
        {this.props.infoBoxIsVisible && (
          <InfoBox onCloseClick={this.handleClick}>
            <div style={infoBoxStyle}>
              <div style={styles.ownerText}>
                <b>Owner</b>: {startCase(owner)}
              </div>
              <div style={styles.addressText}>
                <b>Address</b>: {Object.values(address).join('\n')}
              </div>
              <br />
              {!insideArea && (
                <div style={styles.warningText}>
                  Warning — This object is outside working area
                </div>
              )}
            </div>
          </InfoBox>
        )}
      </MapMarker>
    )
  }
}

Marker.propTypes = {
  owner: PropTypes.string.isRequired,
  address: PropTypes.object.isRequired,
  airbnbId: PropTypes.number.isRequired,
  infoBoxIsVisible: PropTypes.bool.isRequired,
  insideArea: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  position: PropTypes.shape({
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
}

Marker.defaultProps = {
  position: {},
}


const styles = {
  infoBoxContainer: {
    backgroundColor: '#dbe46e',
    opacity: 0.75,
    padding: '12px',
    zIndex: -1,
    position: 'relative',
  },
  ownerText: {
    fontSize: '15px',
  },
  addressText: {
    fontSize: '13px',
    width: 200,
  },
  warningText: {
    color: 'red',
    fontSize: '16px',
    fontWeight: 'bold',
  },
}
