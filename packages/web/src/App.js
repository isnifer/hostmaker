import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from 'shared/store'

import enhanceRoot from 'shared/screens/Root'
import Root from './pages/Root'

const EnhancedRoot = enhanceRoot(Root)

render(
  <Provider store={store}>
    <EnhancedRoot />
  </Provider>,
  document.getElementById('app')
)
