import { hot } from 'react-hot-loader'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Geocoder from 'react-native-geocoding'
import startCase from 'lodash/startCase'
import { mapArea, googleMapKey } from 'shared/constants'
import testID from 'shared/utils/testID'
import Address from '../components/Address'
import Spinner from '../components/Spinner'
import GoogleMap from '../components/GoogleMap'

Geocoder.setApiKey(googleMapKey)

const columns = [
  { id: 'owner', mapValue: apartment => startCase(apartment.owner) },
  { id: 'address', mapValue: apartment => <Address address={apartment.address} /> },
  { id: 'incomeGenerated', mapValue: apartment => `${apartment.incomeGenerated}£` },
]

/*
For this exercise the property details have following fields:
1) Owner(Required)
2) Address
  a. Line1 (Required)
  b. Line2 (Optional)
  c. Line3 (Optional)
  d. Line4 (Required)
  e. Postcode (Required)
  f. City (Required)
  g. Country (Required)
3) IncomeGenerated (Required)
 */

const googleMapCenter = {
  lat: mapArea.latitude,
  lng: mapArea.longitude,
  radius: mapArea.radius,
}

class App extends Component {
  markers = {}

  async componentDidUpdate(prevProps) {
    const { apartments } = this.props

    if (apartments && prevProps.apartments.length !== apartments.length) {
      for (const apartment of apartments) {
        const fullAddress = Object.values(apartment.address).join(' ')
        const { results } = await Geocoder.from(fullAddress)

        const currentLocation = results[0].geometry.location

        this.markers[apartment.airbnbId] = currentLocation
      }

      this.forceUpdate() // Pss, don't tell anyone. For challenge only
    }
  }

  render() {
    const { apartments } = this.props

    if (!apartments.length) {
      return (
        <Spinner />
      )
    }

    const markers = apartments.map(apartment => ({
      ...apartment,
      position: this.markers[apartment.airbnbId],
    }))

    return (
      <div>
        <table style={styles.table}>
          <thead>
            <tr>
              {columns.map(column => (
                <th key={column.id} style={styles.td}>{startCase(column.id)}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {apartments.map(apartment => (
              <tr key={apartment.airbnbId}>
                {columns.map((column) => {
                  const uniqId = `${apartment.airbnbId}_${column.id}`

                  return (
                    <td key={uniqId} style={styles.td} {...testID(uniqId)}>
                      {column.mapValue(apartment)}
                    </td>
                  )
                })}
              </tr>
            ))}
          </tbody>
        </table>
        <GoogleMap
          center={googleMapCenter}
          markers={markers}
          loadingElement={<div style={{ height: '100%' }} />}
          containerElement={<div style={{ width: 900, height: 400 }} />}
          mapElement={<div style={{ height: '100%' }} />}
        />
      </div>
    )
  }
}

App.propTypes = {
  apartments: PropTypes.array.isRequired,
}

const styles = {
  table: {
    width: 900,
    borderCollapse: 'collapse',
    marginBottom: 8,
  },
  td: {
    padding: 5,
    verticalAlign: 'top',
    border: '1px solid #C3C3C3',
    textAlign: 'left',
  },
}

export default hot(module)(App)
