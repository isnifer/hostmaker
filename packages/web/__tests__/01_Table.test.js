import startCase from 'lodash/startCase'
import store from 'shared/store'
import { addressParamsOrder } from 'shared/constants'
import { loadApartments } from 'api/actions'
import test from './utils/tape'

test('01_Table', async (t, driver) => {
  await driver.url('/')

  await store.dispatch(loadApartments())
  const { apartments } = store.getState().apartments

  const columns = [
    {
      id: 'owner',
      value: apartment => startCase(apartment.owner),
    },
    {
      id: 'address',
      value: apartment => addressParamsOrder.reduce((memo, item) => {
        const content = apartment.address[item]

        return content ? memo.concat(content) : memo
      }, []).join('\n'),
    },
    {
      id: 'incomeGenerated',
      value: apartment => `${apartment.incomeGenerated}£`,
    },
  ]

  for (const column of columns) {
    for (const apartment of apartments) {
      const testId = `${apartment.airbnbId}_${column.id}`

      await driver.waitForVisible(testId)
      t.pass(`${testId} is visible`)

      await driver.toHaveText(testId, column.value(apartment), `${testId} content as expected`)
    }
  }
})
