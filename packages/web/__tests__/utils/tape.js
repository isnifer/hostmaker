import test from 'tape-async'
import prometheus from './Prometheus'

export default function tape(name, callback) {
  return test(name, async (t) => {
    const driver = await prometheus.init(t)

    try {
      await callback(t, driver)
    } finally {
      await driver.driver.end()
    }
  })
}
