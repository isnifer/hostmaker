import { remote } from 'webdriverio'

const DEFAULT_DELAY = 5000

class Prometheus {
  init = async (t) => {
    const options = {
      desiredCapabilities: {
        browserName: 'chrome',
      },
      host: '0.0.0.0',
      baseUrl: 'http://isnifer-hostmaker.ddns.net',
      bail: 1,
    }

    this.t = t

    this.driver = remote(options)

    await this.driver.init()
    await this.driver.setViewportSize({ width: 1920, height: 1080 })

    return this
  }

  url = url => this.driver.url(url)

  waitForVisible = (selector, shouldBeHidden = false, timeout = DEFAULT_DELAY) => (
    this.driver.waitForVisible(this.idFromQA(selector), timeout, shouldBeHidden)
  )

  click = selector => this.driver.click(this.idFromQA(selector))

  waitForClick = async (selector, timeout = DEFAULT_DELAY) => {
    await this.waitForVisible(selector, timeout)
    await this.click(selector)
  }

  url = url => this.driver.url(url)

  toHaveText = async (selector, expectedValue, message) => {
    const text = await this.driver.getText(this.idFromQA(selector))
    return this.t.equal(text, expectedValue, message)
  }

  idFromQA = id => `[data-qa="${id}"]`
}

export default new Prometheus()
