const path = require('path')

module.exports = {
  mode: process.env.MODE || 'production',
  entry: [
    '@babel/polyfill',
    './src/App.js',
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/',
  },

  resolve: {
    extensions: ['.js'],
    symlinks: false,
  },

  devtool: 'source-map',

  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules\/(?!api|shared)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          plugins: [
            ['@babel/plugin-proposal-decorators', { legacy: true }],
            ['@babel/plugin-proposal-pipeline-operator', { proposal: 'minimal' }],
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-proposal-do-expressions',
            '@babel/plugin-proposal-export-default-from',
            '@babel/plugin-proposal-export-namespace-from',
            '@babel/plugin-proposal-json-strings',
            '@babel/plugin-syntax-dynamic-import',
            '@babel/plugin-syntax-import-meta',
            'react-hot-loader/babel',
          ],
        },
      },
    }],
  },

  target: 'web',

  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    historyApiFallback: true,
  },
}
