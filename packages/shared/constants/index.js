export const mapArea = {
  latitude: 51.5073835,
  longitude: -0.1277801,
  latitudeDelta: 0.6,
  longitudeDelta: 0.6,
  radius: 20000,
}

export const googleMapKey = 'AIzaSyDVNpa3VRzDxkE4368XVJc7CjGmjeAZH5w'

export const addressParamsOrder = [
  'line1',
  'line2',
  'line3',
  'line4',
  'postCode',
  'city',
  'country',
]
