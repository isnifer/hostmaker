import React, { Component } from 'react'
import PropTypes from 'prop-types'
import hoistStatics from 'hoist-non-react-statics'
import { connect } from 'react-redux'
import { loadApartments } from 'api/actions'

export default function enhanceRoot(ComposedComponent) {
  class RootHOC extends Component {
    static propTypes = {
      loadApartments: PropTypes.func.isRequired,
    }

    componentDidMount() {
      this.props.loadApartments()
    }

    render() {
      return (
        <ComposedComponent {...this.props} />
      )
    }
  }

  const mapStateToProps = ({ apartments }) => apartments

  return connect(mapStateToProps, { loadApartments })(
    hoistStatics(RootHOC, ComposedComponent)
  )
}
