import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { apartments } from 'api/reducers'

const combinedReducers = combineReducers({
  apartments,
})

const middleware = [thunk]

if (process.env.NODE_ENV === 'development' || typeof __DEV__ !== 'undefined') {
  middleware.push(createLogger({ collapsed: true }))
}

const initialState = {}

export default createStore(
  combinedReducers,
  initialState,
  compose(applyMiddleware(...middleware))
)
