export default function testID(id) {
  return { 'data-qa': id }
}
