import React from 'react'
import { View, ActivityIndicator, StyleSheet } from 'react-native'

export default function Spinner() {
  return (
    <View style={styles.wrapper}>
      <ActivityIndicator />
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
