import React from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'

const addressParams = [
  ['line1', PropTypes.string.isRequired],
  ['line2', PropTypes.string],
  ['line3', PropTypes.string],
  ['line4', PropTypes.string.isRequired],
  ['postCode', PropTypes.string.isRequired],
  ['city', PropTypes.string.isRequired],
  ['country', PropTypes.string.isRequired],
]

export default function Address({ address }) {
  return addressParams.reduce((memo, [item]) => {
    const currentProperty = address[item]

    return !currentProperty ? memo : memo.concat(
      <View key={item}>
        <Text>{currentProperty}</Text>
      </View>
    )
  }, [])
}

Address.propTypes = {
  address: PropTypes.shape(
    addressParams.reduce(
      (memo, [key, value]) => ({ ...memo, [key]: value }), {}
    )
  ).isRequired,
}
