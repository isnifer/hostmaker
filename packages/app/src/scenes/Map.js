import React, { Component } from 'react'
import { StyleSheet, InteractionManager } from 'react-native'
import PropTypes from 'prop-types'
import MapView, { Marker, Circle } from 'react-native-maps'
import Geocoder from 'react-native-geocoding'
import startCase from 'lodash/startCase'
import { mapArea, googleMapKey } from 'shared/constants'
import Spinner from '../components/Spinner'

Geocoder.setApiKey(googleMapKey)

export default class MapScreen extends Component {
  static navigationOptions = {
    title: 'Map',
  }

  state = {
    region: mapArea,
    isMapReady: false,
    isMarkersLoaded: false,
  }

  markers = {}

  async componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({ isMapReady: true })
    })

    const apartments = this.props.navigation.getParam('apartments')
    for (const apartment of apartments) {
      const fullAddress = Object.values(apartment.address).join(' ')
      const { results } = await Geocoder.from(fullAddress)

      const currentLocation = results[0].geometry.location

      this.markers[apartment.airbnbId] = {
        latitude: currentLocation.lat,
        longitude: currentLocation.lng,
      }
    }

    this.setState({ isMarkersLoaded: true })
  }

  render() {
    if (!this.state.isMapReady) {
      return (
        <Spinner />
      )
    }

    const apartments = this.props.navigation.getParam('apartments')
    const { isMarkersLoaded } = this.state

    const { latitude, longitude, radius } = this.state.region

    return (
      <MapView
        rotateEnabled={false}
        style={styles.map}
        initialRegion={this.state.region}>
        <Circle
          center={{ latitude, longitude }}
          radius={radius}
          strokeColor="#005005"
          fillColor="rgba(165, 214, 167, .2)"
        />
        {isMarkersLoaded && apartments.map(apartment => (
          <Marker
            key={apartment.airbnbId}
            coordinate={this.markers[apartment.airbnbId]}
            title={startCase(apartment.owner)}
            description={Object.values(apartment.address).join(', ')}
          />
        ))}
      </MapView>
    )
  }
}

MapScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
})
