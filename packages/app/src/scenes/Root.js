import React, { Component } from 'react'
import { View, Text, Button, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import startCase from 'lodash/startCase'
import Address from '../components/Address'
import Spinner from '../components/Spinner'

const columns = [
  {
    id: 'owner',
    mapValue: apartment => (
      <Text>{startCase(apartment.owner)}</Text>
    ),
  },
  {
    id: 'address',
    mapValue: apartment => (
      <Address address={apartment.address} />
    ),
  },
  {
    id: 'incomeGenerated',
    mapValue: apartment => (
      <Text style={styles.incomeText}>{apartment.incomeGenerated}£</Text>
    ),
  },
]

/*
For this exercise the property details have following fields:
1) Owner(Required)
2) Address
  a. Line1 (Required)
  b. Line2 (Optional)
  c. Line3 (Optional)
  d. Line4 (Required)
  e. Postcode (Required)
  f. City (Required)
  g. Country (Required)
3) IncomeGenerated (Required)
 */


export default class RootScreen extends Component {
  static navigationOptions = {
    title: 'Rent Management',
  }

  location = {}

  handleMapButtonPress = () => {
    this.props.navigation.navigate('Map', {
      apartments: this.props.apartments,
    })
  }

  render() {
    const { apartments } = this.props

    if (!apartments || !apartments.length) {
      return (
        <Spinner />
      )
    }

    return (
      <View style={styles.wrapper}>
        <View style={styles.table}>
          <View style={styles.tr}>
            {columns.map(column => (
              <View key={column.id} style={[styles.td, styles.tdHead]}>
                <Text style={styles.headTitle}>{startCase(column.id)}</Text>
              </View>
            ))}
          </View>
          {apartments.map(apartment => (
            <View key={apartment.airbnbId} style={styles.tr}>
              {columns.map(column => (
                <View key={`${apartment.airbnbId}_${column.id}`} style={styles.td}>
                  {column.mapValue(apartment)}
                </View>
              ))}
            </View>
          ))}
        </View>
        <Button
          title="See these apartments on the map"
          onPress={this.handleMapButtonPress}
        />
      </View>
    )
  }
}

RootScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
  apartments: PropTypes.array.isRequired,
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  table: {
    marginBottom: 40,
  },
  tr: {
    flexDirection: 'row',
  },
  td: {
    flex: 1,
    padding: 5,
    borderWidth: 1,
    borderColor: '#C3C3C3',
    minHeight: 100,
  },
  tdHead: {
    minHeight: 20,
  },
  headTitle: {
    fontWeight: 'bold',
    fontSize: 12,
  },
  incomeText: {
    color: 'green',
  },
})
