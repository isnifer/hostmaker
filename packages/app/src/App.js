import React from 'react'
import { createStackNavigator } from 'react-navigation'
import { Provider } from 'react-redux'
import ehnanceRoot from 'shared/screens/Root'
import store from 'shared/store'
import RootScreen from './scenes/Root'
import MapScreen from './scenes/Map'

const AppNavigator = createStackNavigator(
  { Root: ehnanceRoot(RootScreen), Map: MapScreen },
  { initialRouteName: 'Root' }
)

export default () => (
  <Provider store={store}>
    <AppNavigator />
  </Provider>
)
