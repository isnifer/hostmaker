const path = require('path')

module.exports = {
  extraNodeModules: {
    api: path.resolve(__dirname, '..', 'api'),
    shared: path.resolve(__dirname, '..', 'shared'),
  },
  projectRoot: path.resolve(__dirname),
  watchFolders: [
    path.resolve(__dirname, '..', 'api'),
    path.resolve(__dirname, '..', 'shared'),
  ],
}
