import generateActionTypes from '../utils/generateActionTypes'

// eslint-disable-next-line
export const LOAD_APARTMENTS = generateActionTypes('LOAD_APARTMENTS')
