const CONTENT_TYPE_APPLICATION_JSON = 'application/json'
const CONTENT_TYPE_HEADER = 'Content-Type'

const SERVER_HOST = 'http://isnifer-hostmaker.ddns.net'
const getEndpoint = url => `${process.env.API_URL || SERVER_HOST}/${url}`

export default function network(method, url) {
  const options = {
    method,
    credentials: 'same-origin',
    headers: {
      Accept: CONTENT_TYPE_APPLICATION_JSON,
      [CONTENT_TYPE_HEADER]: CONTENT_TYPE_APPLICATION_JSON,
    },
  }

  return fetch(getEndpoint(url), options)
    .then((response) => {
      const contentType = response.headers.get(CONTENT_TYPE_HEADER)
      if (!contentType.includes(CONTENT_TYPE_APPLICATION_JSON)) {
        return { json: null, response }
      }

      return response.json().then(json => ({ json, response }))
    })
    .then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject({
          json,
          statusCode: response.status,
          statusText: response.statusText,
        })
      }

      return json
    })
}
