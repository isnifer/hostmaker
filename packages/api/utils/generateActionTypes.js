export const suffixes = ['LOADING', 'SUCCESS', 'ERROR']

export default actionType => suffixes.reduce((memo, suffix) => ({
  ...memo,
  [suffix]: `${actionType}_${suffix}`,
}), {})
