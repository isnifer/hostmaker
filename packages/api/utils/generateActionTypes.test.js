import crypto from 'crypto'
import test from 'tape'
import generateActionTypes, { suffixes } from './generateActionTypes'

test('generateActionTypes', (t) => {
  const randomString = crypto.createHash('md5').update('actionType').digest('hex')

  const expectedValues = {
    [suffixes[0]]: `${randomString}_${suffixes[0]}`,
    [suffixes[1]]: `${randomString}_${suffixes[1]}`,
    [suffixes[2]]: `${randomString}_${suffixes[2]}`,
  }

  t.same(generateActionTypes(randomString), expectedValues, 'actionTypes should be as expected')

  t.end()
})
