import { LOAD_APARTMENTS } from '../constants/actionTypes'
import network from '../network'

// eslint-disable-next-line
export const loadApartments = () => async (dispatch) => {
  dispatch({ type: LOAD_APARTMENTS.LOADING })

  try {
    const apartments = await network('GET', 'apartments')
    dispatch({ type: LOAD_APARTMENTS.SUCCESS, payload: { apartments } })
  } catch (error) {
    dispatch({ type: LOAD_APARTMENTS.ERROR, error })
  }
}
