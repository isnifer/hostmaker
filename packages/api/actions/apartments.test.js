import test from 'tape-async'
import configureStore from 'redux-mock-store'
import nock from 'nock'
import thunk from 'redux-thunk'
import { loadApartments } from './apartments'

const mockedResponse = [
  {
    owner: 'carlos',
    airbnbId: 3512500,
  },
  {
    owner: 'ankur',
    airbnbId: 1334159,
  },
  {
    owner: 'elaine',
    airbnbId: 12220057,
  },
]


test('apartments actions', async (t) => {
  const mockStore = configureStore([thunk])
  const store = mockStore({})

  nock(process.env.API_URL)
    .persist()
    .defaultReplyHeaders({
      'Content-Type': 'application/json',
    })

    .get('/apartments')
    .reply(200, mockedResponse)

  await store.dispatch(loadApartments())

  const [, successAction] = store.getActions()

  t.same(
    successAction.payload,
    { apartments: mockedResponse },
    'payload should contain a response from server'
  )
})
