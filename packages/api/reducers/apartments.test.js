import test from 'tape'
import { LOAD_APARTMENTS } from '../constants/actionTypes'
import apartmentsReducer, { initialState } from './apartments'

test('apartments reducer', (t) => {
  t.same(
    apartmentsReducer(undefined, {}),
    initialState,
    'should return initialState on unknown action'
  )

  t.same(
    apartmentsReducer(undefined, { type: LOAD_APARTMENTS.LOADING }),
    { ...initialState, isLoading: true },
    'should update "isLoading" param in state on loading'
  )

  const apartments = [1, 2, 3]
  t.same(
    apartmentsReducer(undefined, { type: LOAD_APARTMENTS.SUCCESS, payload: { apartments } }),
    { ...initialState, isLoading: false, apartments },
    'should update "isLoading, apartments" params in state on success'
  )

  const error = 'Cannot load apartments'
  t.same(
    apartmentsReducer(undefined, { type: LOAD_APARTMENTS.ERROR, payload: { error } }),
    { ...initialState, isLoading: false, error },
    'should update "isLoading, error" params in state on success'
  )

  t.end()
})
