import { LOAD_APARTMENTS } from '../constants/actionTypes'

export const initialState = {
  isLoading: false,
  error: null,
  apartments: [],
}

export default function apartments(state = initialState, { type, payload }) {
  if (type === LOAD_APARTMENTS.LOADING) {
    return {
      ...state,
      isLoading: true,
    }
  }

  if (type === LOAD_APARTMENTS.SUCCESS) {
    return {
      ...state,
      isLoading: false,
      apartments: payload.apartments || [],
    }
  }

  if (type === LOAD_APARTMENTS.ERROR) {
    return {
      ...state,
      isLoading: false,
      error: payload ? payload.error : true,
    }
  }

  return state
}
