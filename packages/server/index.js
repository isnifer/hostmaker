import path from 'path'
import express from 'express'
import cors from 'cors'
import data from './data.json'

const app = express()
const port = 80

app.use(cors())

const staticFolder = path.resolve(__dirname, '../web/dist')
app.use(express.static(staticFolder))

app.use((req, res, next) => {
  res.header('Content-Type', 'application/json')
  next()
})

// JSON
app.get('/apartments', (req, res) => {
  res.send(JSON.stringify(data))
})

// ALL OTHER ONES
app.get('*', (req, res) => {
  res.sendStatus(404)
})

app.listen(port)
