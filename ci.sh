#!/bin/bash

set -e

rm -rf node_modules

npm i

npm run clean:bootstrap

npm run api:test

npm run web:build

npm run server:start
